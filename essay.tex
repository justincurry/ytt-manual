\documentclass[12pt,reqno]{amsproc}
%%%%%%%%%%%%%
%%%%%% Copied from Shortcuts-JMC
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}

\usepackage{enumitem}
\newlist{legal}{enumerate}{10}
\setlist[legal]{label*=\arabic*.}

\setlength{\parskip}{1em}

\title{Yoga Essay}
\author{Justin Michael Curry}

\begin{document}

\maketitle

I was led to the yoga teacher training program at Global Breath Studio because I was in a deep state of an emotional, physical, and spiritual crisis.
In 2009 at the age of 23, I prided myself on my internal compass, on having a sense of direction in life that would lead me out of whatever spiritual woods I had found myself in.
However, by December of 2016 and at the age of 30, I was writing in an email to a person that I had hurt deeply that
\begin{quote}
\em{``I find myself in some unfamiliar woods with only a few matches left. What has caused me to come undone? Where did I lose the path?''}
\end{quote}
Simply put: I had forgotten how to live.

In many ways it is more likely that I had never known how to live and that the spiritual woods I had felt in my early 20s were simple brush when compared to the jungle of despair that I had run into in the wake of serious trauma that I experienced in my 27th through 30th years.
I was drowning in karmic quicksand and it is by divine intervention that Bart Westdorp, Nina Be, and my new sangha---the constituents of the 20th MindBody Centering Yoga (MBCY) Yoga treacher training---that I was set on a new path, with some helpful sign posts along the way.

Although this is perhaps unsurprising, the path that I've been set on includes religious elements and hints of the supernatural.
Although many of the aspects of the Yoga Teacher Training could be interpreted as psychosomatic techniques\footnote{cf. Bryant's characterization of Patanjali's Yoga Sutras, p. xlv} for conceptualizing ones internal psychology without the need for a religious framework, the book \emph{Autobiography of a Yogi} (AOAY) by Paramahansa Yogananda is explicitly supernatural and has all the trademarks of a religious text.
These trademarks run the gamut from 1) simple anecdotes and lessons on how to live a good life, to 2) intense, personal experiences that could be described as psychedelic experiences, to 3) descriptions of magic and miracles requiring a metaphysics not currently supported by a western scientific worldview,

Whether it is AOAY or the Judeo-Christian Bible, I've often wondered why people need miracles to justify spiritual pursuits.
After all, shouldn't we take up these pursuits to enjoy the benefits they give in the here and now, contenting ourselves solely with the practical advantages offered by the first category above, with no further suppositions or requirements of faith?
In many ways this is approach offered by Buddhism.
Indeed, the Parable of the Poisoned Arrow\footnote{\url{https://en.wikipedia.org/wiki/Parable_of_the_Poisoned_Arrow}} describes a man who has been shot by an arrow and the doctor wants to remove the arrow immediately to help alleviate the man's suffering and save his life.
However, the man does not want the arrow removed until his questions are answered: ``Who shot this arrow? What is his age? Who are his parents? Why did he shoot it?''
If the man waits for the answers to each of these questions, then he will likely die.
The implication of this parable is that if a person, like me, is experiencing a great deal of suffering and waits to have all of their metaphysical questions---where the universe came from, whether it is eternal, whether there is a god, and why we are here on Earth---answered before accepting the help of a spiritual teacher, then they will continue to suffer, experience existential dread, and run the risk of dying a premature death brought about by their own hand.

Continuing the Parable of the Poisoned Arrow one can ask 
``Who or what is the doctor that offers to remove the arrow of our suffering?''
A Buddhist will likely answer that the doctor is, by analogy, adherence to The Noble Eightfold Path.
However, my experience with Buddhism made most of the Eightfold Way feel like a set of abstract beliefs with the only physical practice being sitting quietly in meditation.
In college, I would do two hour sits once a week at the Kwan Um Zen Center in Cambridge, Massachussetts.
These sits would often feel exteme and anxiety-provoking at times, like sitting in a psychological fire that you hoped would burn your ego to a crisp and allow your sense of self, like a pile of ash, to be blown to the wind.

It was perhaps a prophecy of things to come when in 2008-2009 I shared a row house in West Philadelphia with a recently divorced woman, who said that her anxiety and jitteriness made meditation nearly impossible and that yoga was her current medicine of choice.
In hindsight, a diversity of therapies---meditation and/or asana---makes sense. 
Suffering is a common symptom of many different spiritual diseases.
Although in abstract these diseases can be traced to the fundamentals of attachment or the fluctuating states of mind, such a reductionist approach can become impractical.
In reality, these fundamentals organize themselves into other identifiable causes and layers to suffering: neglect, abuse, trauma, anxiety, depression, acting out, misbehavior, and so on.

My own trauma was tied to a deep sense of undesirability.
I self-medicated with exercise in order to feel desirable.
Although this worked for some time and helped bring me part way out of a deep depressive episode, it was fundamentally unbalanced and led to its own side-effects.
Although numerically equivalent to the Buddha's prescription, I have found the Eight Limbs of Yoga or \emph{Ashtanga Yoga}\footnote{``Ash'' meaning eight and ``tanga'' meaning limbs}---consisting of \emph{yamas, niyamas, asana, pranayama, pratyahara, dharana, dhyana,} and \emph{samadhi}---to be a much more powerful medicine and the ultimate guide to living a good life that includes meditation (dhayana) as just one part of a more complete system of therapies.

The appeal of Ashtanga Yoga to my mind is its logical structure.
The yamas are guidelines for conduct with regards to other people.
Whatever else might be wrong in your life your conduct with regards to the external world should follow five \emph{yamas}: 1) \emph{ahimsa}---practice non-violence and reduce harm; 2) \emph{satya}---be truthful, but take into account harm caused by truth; 3) \emph{asetya}---refrain from stealing people's property, money, or time; 4) \emph{brahmacharya}---moderate sexual conduct and the amount of time or energy expended in the pursuit of fleeting pleasures; 5) \emph{aparigraha}---refrain from greed and hoarding.
Having shored up your conduct with others, you can then clean up your internal house by following the five \emph{niyamas}, which are personal guidelines for living: 1) \emph{saucha}---practice good hygiene by eating right, keeping the body clean, and maintaining a clean living space; 2) \emph{santosha}---count your blessings, practice contentment, and do not covet; 3) \emph{tapas}---use your energy appropriately to either create change in yourself through hard work or preserve your life-force by turning down efforts appropriately; 4) \emph{svadhyaya}---feed the mind and soul through school or scripture or by practicing introspection; 5) \emph{ishvara-pranidhana}---devote yourself to something bigger than you---your community, your planet, your personal god.
Checking the basics of your conduct with others (\emph{yamas}) and your conduct with yourself (\emph{niyamas}), you can use physical practice and exercise (\emph{asana}) to purify your mind.
It is remarkable how much better you can feel by simplying getting outside and sweating or by getting on your mat and doing what is popularly known as yoga.
The further practice of breathing exercises (\emph{pranayama}) is also remarkably helpful for modulating stress or chronic fatigue and depression.
The remaining four branches of yoga---withdrawal from the senses (\emph{pratyahara}), concentration (\emph{dharana}), meditation (\emph{dhyana}), and full meditative absorption (\emph{samadhi})---are in some ways more advanced concerned primarily with soterological or liberation-inducing techniques


However, just as meditation may be too limited for some, Ashtanga Yoga can be too broad for others.



% This is indeed what I tried earlier in my life, but my time at the Kwan Um Zen Center in Cambridge, Massachussetts led me to believe that following this path largely consists of meditation.
% When I tried applying this to my 

% Many of my experiences in the Yoga Teacher Training program have caused me to question and soften on the hard-and-fast atheism that I developed in middle school as a response to the overt and oppressive Christianity that I had faced in some of my neighbors, teachers, and members of the ``majority'' of rural Virgina.
% It is truly fortunate that my parents and my grandmother provided space for me to find my own spirituality and philosophy in books.
% The name-calling in school in Harrisonburg, Virginia and the experiences of being spit on in my urban Virginia Beach neighborhood that for contradicting a Christian worldview were not present at home.
% Due to the space that I had at home to inquire, I am able as an adult to suspend disbelief at times and, most recently, entertain some of the stories described in \emph{Autobiography of a Yogi} by Paramhansa Yogananda. at least 

% at least entertain magic and shamanism in my papasan chair while reading ``The Teachings of Don Juan'' at the age of 15, without feeling a knee-jerk reaction to reject onsumption of entheogens in college gave me my first glimpse at my \emph{purusha}---my soul stripped of all externally-imposed identity.


% As far too few know, Yoga is a practice with deep spiritual roots.
% Yoga is now considered one of the six orthodox schools of Indian philosophy [Bryant, p.xxxii], but yogic practices have been practiced in one form or another on the Indian subcontinent for over four thousand years\footnote{As noted in [Bryant, p. xx], archaeological excavations in the Indus valley unearthed a seal with a figure ``seated with arms extended and resting on the knees in a classical meditative posture.''}.



\end{document}